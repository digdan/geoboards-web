import React from 'react';
import PropTypes from 'prop-types'
import { withRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux'
import Loadable from 'react-loadable'
import NotFound from './Pages/Home/NotFound'
import _ from 'lodash'
import '../node_modules/antd/dist/antd.less';

const loading = props => {
  if (props.error) {
    return <div>Error!</div>;
  } else if (props.pastDelay) {
    return <div>Loading...</div>;
  } else {
    return null;
  }
}

const Home = Loadable({
  loader: () => import('./Pages/Home/Home'),
  loading,
  delay: 300
})
const AboutUs = Loadable({
  loader: () => import('./Pages/Home/AboutUs'),
  loading,
  delay: 300
})
const Community = Loadable({
  loader: () => import('./Pages/Home/Community'),
  loading,
  delay: 300
})
const Register = Loadable({
  loader: () => import('./Pages/Home/Register'),
  loading,
  delay: 300
})
const Login = Loadable({
  loader: () => import('./Pages/Home/Login'),
  loading,
  delay: 300
})
const Geo = Loadable({
  loader: () => import('./Pages/Geo'),
  loading,
  delay:300
})



class App extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    if (_.get(this.props, 'loaded', false) === true) {
      return (
        <div className="App">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/aboutus" component={AboutUs} />
            <Route exact path="/community" component={Community} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/geo" component={Geo} />
            <Route path="*" component={NotFound} />
          </Switch>
        </div>
      )
    } else {
      return(
        <div>
          <div id="appLoading"/>
          <div id="appLoadingText">Connecting to Application</div>
        </div>
      );
    }
  }
}

App.propTypes = {
  loaded: PropTypes.bool
}

App.defaultProps = {
  loaded: false
}

const mapStateToProps = state => {
  return {
    loaded: state.states.loaded,
    theme: state.ui.theme
  };
};

export default withRouter(connect(
  mapStateToProps,
  null
)(App))

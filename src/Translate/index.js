import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import en from './en.json'

const messages = {
  en
}

class TranslateProvider extends React.Component {
  static propTypes = {
    locale: PropTypes.string.isRequired
  }
  // you must specify what you’re adding to the context
  static childContextTypes = {
    t: PropTypes.func.isRequired
  }
  getChildContext() {
    console.log('Rendering in language', this.props.locale)
    const t = message => {
      let locale = this.props.locale
      if (!_.has(messages, locale)) {
        locale = 'en'
      }
      return messages[locale][message]
    }
    return { t }
  }
  render() {
    return React.Children.only(this.props.children)
  }
}

TranslateProvider.propTypes = {
  children: PropTypes.element
}

TranslateProvider.defaultProps = {
  children: []
}

const translate = ComponentToWrap => {
  return class TranslateComponent extends React.Component {
    static contextTypes = {
      t: PropTypes.func.isRequired
    }
    render() {
      const { t } = this.context
      return (
        <ComponentToWrap {...this.props} t={t} />
      )
    }
  }
}

const getLocale = defaultLocale => {
  let rawLocale = 'en-US'
  if (typeof window.navigator.languages !== 'undefined') {
    rawLocale = window.navigator.languages[0];
  } else if (typeof window.navigator.language !== 'undefined') {
    rawLocale = window.navigator.language
  } else {
    rawLocale = defaultLocale
  }
  const localeParts = rawLocale.split('-')
  return localeParts[0]
}

export {
  TranslateProvider,
  translate,
  getLocale
}

import React from 'react'
import PropTypes from 'prop-types'
import {themed} from '../../Themes'
import {translate} from '../../Translate'
import './Header.styl'

class Header extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const classNames = [
      this.props.theme.Header,
      'Header'
    ]
    return(
      <div className={classNames.join(' ')}>
        {this.props.t('hello')}
      </div>
    );
  }
}

Header.propTypes = {
  theme: PropTypes.object,
  t: PropTypes.func
}

Header.defaultProps = {
  theme: {},
  t: ()=>{}
}


export default themed(translate(Header))

import React from 'react';

class Channel extends React.Component {

  render() {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100vh'
        }}
      >
        Channel {this.props.key}
      </div>
    );
  }
}

export {
  Channel as default
}

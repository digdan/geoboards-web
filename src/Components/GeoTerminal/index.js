import React from 'react';
import Terminal from 'terminal-in-react';
import * as actions from '../../Reducers/actions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { auth } from '../../Modules/user'


//TODO -- integrate into WEB Sockets

class GeoTerminal extends React.Component {

  dispatch = action => {
    this.props.terminalDispatch(action)
  }

  getState = () => {
    this.dispatch({
      type: actions.COMMAND_STATE_SHOW
    })
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100vh'
        }}
      >
        <Terminal
          descriptions={{ color: false, show: false, clear: false }}
          hideTopBar={true}
          allowTabs={false}
          prompt={'Geo>'}
          color={'black'}
          backgroundColor={'#fff'}
          startState={'maximised'}
          watchConsoleLogging
          style={{ fontWeight: 'normal', fontSize: '1em' }}
          commands={{
            'open-google': () => window.open('https://www.google.com/', '_blank'),
            showmsg: this.showMsg,
            'getState' : this.getState,
            popup: () => window.alert('Terminal in React')
          }}
          msg=''
        />
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  terminalDispatch: data => actions.terminalDispatch(data)
}, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(auth(GeoTerminal));

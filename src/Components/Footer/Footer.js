import React from 'react'
import PropTypes from 'prop-types'
import { themed } from '../../Themes'
import './Footer.styl'

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const classNames = [
      this.props.theme.Footer,
      'Footer'
    ]
    return(
      <div className={classNames.join(' ')}>
        Footer
      </div>
    );
  }
}

Footer.propTypes = {
  theme: PropTypes.object,
  t: PropTypes.func
}

Footer.defaultProps = {
  theme: {},
  t: ()=>{}
}

export default themed(Footer)

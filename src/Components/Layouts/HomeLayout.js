import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, NavLink } from 'react-router-dom'
import { Layout, Menu, Breadcrumb } from 'antd';
import _ from 'lodash'
const { Header, Content, Footer } = Layout;

const navItems = [
  {
    id:1,
    link:'/',
    label:'Home'
  },
  {
    id:2,
    link:'/aboutus',
    label:'About Us'
  },
  {
    id:3,
    link:'/community',
    label:'Community'
  },
  {
    id:4,
    link:'/register',
    label:'Register'
  },
  {
    id:5,
    link:'/login',
    label:'Login'
  }
]

class HomeLayout extends React.Component {
  constructor(props) {
    super(props)
  }
  render = () => {
    const selected = [_.find(navItems, {link:this.props.match.path}).id.toString()]
    return(
      <Layout className='homeLayout'>
        <Header style={{background:'#fff'}}>
          <div className="logo" />
          <Menu
            className="mainNav"
            mode="horizontal"
            defaultSelectedKeys={selected}
            style={{ lineHeight: '64px' }}
          >
            {navItems.map( item => {
              return (<Menu.Item key={item.id}><NavLink to={item.link}>{item.label}</NavLink></Menu.Item>)
            })}
          </Menu>
          <Menu
            className="profileNav"
            mode="horizontal"
            style={{ lineHeight: '64px'}}
          >
          </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>
          { this.props.children }
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          IWD Copyright 2018
        </Footer>
      </Layout>
    )
  }
}

HomeLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

HomeLayout.defaultProps = {
  children: null
}

export default HomeLayout;

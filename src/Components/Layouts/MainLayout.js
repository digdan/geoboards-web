import React from 'react'
import PropTypes from 'prop-types'
import {themed} from '../Themes'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'

class MainLayout extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div>
        <Header />
        { this.props.children }
        <Footer />
      </div>
    )
  }
}

MainLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

MainLayout.defaultProps = {
  children: null
}

export default themed(MainLayout);

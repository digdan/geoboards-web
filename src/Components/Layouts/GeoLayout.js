import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, NavLink } from 'react-router-dom'
import { Layout, Menu, Breadcrumb, Tabs } from 'antd';
import { auth } from '../../Modules/user'
import GeoTerminal from '../GeoTerminal'
import Channel from '../../Components/Channel'
import { socketInit } from '../../Reducers/socketActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash'

const { Header, Content, Footer } = Layout;
const TabPane = Tabs.TabPane;

class GeoLayout extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount = () => {
    this.props.socketInit()
  }

  tabulator = () => {
    let tabNum = 1
    let tabs = []
    if (this.props.user.canConsole()) {
      tabs.push( <TabPane className='consoleTab' tab="Console" key={tabNum++}>
        <GeoTerminal/>
      </TabPane> )
      this.props.user.channels.forEach( channel => {
        tabs.push( <TabPane className={`${channel.parent}Tab`} tab={channel.name} key={tabNum++}> <Channel name={channel.key}/> </TabPane> )
      })
    }

    return (<Tabs
      defaultActiveKey="1"
      tabPosition={'left'}
      style={{ height: '100%' }}
    >
      {tabs}
    </Tabs>)
  }
  render = () => {
    return(
      <Layout className='geoLayout'>
        <Header style={{height:'24px', padding:'0px'}}>
          <i>{this.props.user.identity}</i>
          <Menu
            className="profileNav"
            mode="horizontal"
            style={{ lineHeight: '24px', margin:'0px', height:'24px'}}
          >
          </Menu>
        </Header>
        {this.tabulator()}
        <Content style={{ padding: '0 20px' }}>
          { this.props.children }
        </Content>
      </Layout>
    )
  }
}

GeoLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

GeoLayout.defaultProps = {
  children: null
}

const mapDispatchToProps = dispatch => bindActionCreators({
  socketInit: data => socketInit(data)
}, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(auth(GeoLayout));

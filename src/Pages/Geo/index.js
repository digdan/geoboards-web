import React from 'react';
import PropTypes from 'prop-types'
import Layout from '../../Components/Layouts/GeoLayout'
import GeoStyle from './Geo.styl'
import { UserProvider } from '../../Modules/user'
import _ from 'lodash'


class Geo extends React.Component {
  render= () => {
    return(
      <UserProvider>
        <Layout {...this.props} />
      </UserProvider>
    );
  }
}

Geo.contextTypes = {
  store: PropTypes.object
}

export default Geo

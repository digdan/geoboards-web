import React from 'react';
import Terminal from 'terminal-in-react';
import {themed} from '../../Themes'

//TODO -- integrate into WEB Sockets

class GeoTerminal extends React.Component {

  render() {
    return (
      <div className='GeoTerminal'>
        <Terminal
          descriptions={{ color: false, show: false, clear: false }}
          hideTopBar={true}
          allowTabs={false}
          prompt={'Geo>'}
          startState={'maximised'}
          watchConsoleLogging
          style={{ fontFamily: 'Hack, monospaced', fontSize: '1em' }}
          commands={{
            'open-google': () => window.open('https://www.google.com/', '_blank'),
            showmsg: this.showMsg,
            popup: () => window.alert('Terminal in React')
          }}
          msg=''
        />
      </div>
    );
  }
}

export default themed(GeoTerminal)

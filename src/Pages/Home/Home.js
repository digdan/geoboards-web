import React from 'react';
import Layout from '../../Components/Layouts/HomeLayout'

class Home extends React.Component {
  constructor(props) {
    super(props)
  }

  render= () => {
    return(
      <Layout {...this.props}>
        This is the content
      </Layout>
    );
  }
}

export default Home;

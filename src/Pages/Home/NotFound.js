import React from 'react';
import Layout from '../../Components/Layouts/HomeLayout'

class NotFound extends React.Component {
  constructor(props) {
    super(props)
  }

  render= () => {
    return(
      <Layout {...this.props}>
        Page Not Found
      </Layout>
    );
  }
}

export default NotFound;

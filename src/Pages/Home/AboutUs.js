import React from 'react';
import Layout from '../../Components/Layouts/HomeLayout'

class AboutUs extends React.Component {
  constructor(props) {
    super(props)
  }

  render= () => {
    return(
      <Layout {...this.props}>
        AboutUs
      </Layout>
    );
  }
}

export default AboutUs;

import React from 'react';
import Layout from '../../Components/Layouts/HomeLayout'

class Community extends React.Component {
  constructor(props) {
    super(props)
  }

  render= () => {
    return(
      <Layout {...this.props}>
        Community
      </Layout>
    );
  }
}

export default Community;

import React from 'react'
import PropTypes from 'prop-types'

class user {

  constructor( props ) {
    return this.data( props )
  }

  canConsole = () => {
    return true
  }

  data( props ) {
    const user ={
      id: 1,
      access: ['admin'],
      identity: 'digdan',
      location_longitude:-118.3955556,
      location_latitude: 34.0211111,
      channels: [
        {
          parent: 'chat',
          name:'general',
          key:'abc123'
        }
      ],
      canConsole : this.canConsole
    }
    return user
  }
}

class UserProvider extends React.Component {
  /* static propTypes = {
    locale: PropTypes.string.isRequired
  }  */
  static childContextTypes = {
    user: PropTypes.func.isRequired
  }
  getChildContext() {
    return { user }
  }
  render() {
    return React.Children.only(this.props.children)
  }
}

const auth = ComponentToWrap => {
  return class UserComponent extends React.Component {
    static contextTypes = {
      user: PropTypes.func.isRequired
    }
    render() {
      const { user } = this.context
      let userData = new user(...this.props)
      return (
        <ComponentToWrap {...this.props} user={userData} />
      )
    }
  }
}

export {
  UserProvider,
  auth
}

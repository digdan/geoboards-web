import ws from 'ws'
import _ from 'lodash'
import EventEmitter from 'events'
import {eventChannel, END} from 'redux-saga'
import { call, take, put } from 'redux-saga/effects'
export const actions = {
  SOCKET_OPEN : 'socket/open',
  SOCKET_CLOSE : 'socket/close',
  SOCKET_MESSAGE : 'socket/message',
  SOCKET_ERROR : 'socket/error'
}

function * socketSaga(props) {
  const chan = yield call(createEventSocket, props)
  try {
    while (true) {
      let payload = yield take(chan)
      yield put(payload)
    }
  } catch (error) {
    console.log('socket error', error)
  } finally {
    console.log('connection ended')
  }
}

const createEventSocket = props => {
  const socket = new socketController(props)
  return eventChannel(emitter => {
    socket.on(actions.SOCKET_OPEN, data => {
      emitter({type: actions.SOCKET_OPEN})
    })
    socket.on(actions.SOCKET_CLOSE, data => {
      emitter({type: actions.SOCKET_CLOSE})
    })
    socket.on(actions.SOCKET_MESSAGE, data => {
      emitter({type: actions.SOCKET_MESSAGE, data})
    })
    return () => {
      emitter(END)
      socket.close()
    }
  }
  )
}
class socketController extends EventEmitter {
  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      socketOpen: false,
      lastError: null
    }
    this.initSocket({
      url:_.get(this, 'props.url', '//localhost')
    })
  }

  close = () => {
    this.ws.close()
  }

  setState = newState => {
    const oldState = this.state
    //Allow triggers here
    this.state = Object.assign({}, this.state, newState)
  }

  initSocket = props => {
    this.ws = new ws(props.url, {
      perMessageDeflate: false
    })
    this.ws.on('open', () => {
      this.setState({
        socketOpen: true
      })
      this.emit(actions.SOCKET_OPEN, true)
    })
    this.ws.on('close', () => {
      this.setState({
        socketOpen: false
      })
      this.emit(actions.SOCKET_CLOSE, true)
    })
    this.ws.on('message', data => {
      this.emit(actions.SOCKET_MESSAGE, data)
    })
  }

  send = message => {
    this.ws.send(message, error => {
      this.setState({
        lastError: error
      })
    })
  }
}

export {
  socketController,
  createEventSocket,
  socketSaga
}

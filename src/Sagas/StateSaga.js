import { call, select, takeEvery, put, fork } from 'redux-saga/effects'
import * as actions from '../Reducers/actions'
import * as states from '../Reducers/stateActions'
import _ from 'lodash'

function * processCommand(action) {
  switch(action.type) {
    case actions.COMMAND_STATE_SHOW : {
      const state = yield select()
      yield call(console.log, state)
      break;
    }
  }
}

const matchPattern = pattern => {
  return input => {
    return (input.type.indexOf(pattern) > -1)
  }
}

function * watchCommands() {
  yield takeEvery(matchPattern('COMMAND/state_'), processCommand)
}

export function * StateSaga() {
  const state = select()
  if (!_.has(state, 'appState')) {
    yield put({
      type: actions.APP_STATE,
      data: states.STATE_GUEST
    })
  }
  yield fork(watchCommands)
}

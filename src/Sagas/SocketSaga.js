import { call, select, put, all, takeEvery } from 'redux-saga/effects'
import * as actions from '../Reducers/actions'
import * as socketActions from '../Reducers/socketActions'
import * as states from '../Reducers/states'
import _ from 'lodash'

function * onSocketInit(data) {
  yield call(console.log, 'gotSocketInit', data)
}

function * waitSocketInit(data) {
  console.log('waiting for socket init')
  yield takeEvery(socketActions.SOCKET_INIT, onSocketInit, data)
}

export function SocketSaga() {
  console.log('socket saga')
  return [
    waitSocketInit
  ]
}

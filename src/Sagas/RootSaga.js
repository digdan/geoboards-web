import { call, put, all, fork } from 'redux-saga/effects'
import * as actions from '../Reducers/actions'
import { StateSaga } from './StateSaga'
import { SocketSaga } from './SocketSaga'

export function * RootSaga() {
  yield put({
    type: actions.APP_LOADED,
    data: true
  })
  console.log('root saga')
  yield all([
    fork(StateSaga),
    fork(SocketSaga)
  ])
}

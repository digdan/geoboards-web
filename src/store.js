import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware from 'redux-saga'
import { RootReducer } from './Reducers/RootReducer'
import { RootSaga } from './Sagas/RootSaga'

export const history = createHistory();
const sagaMiddleware = createSagaMiddleware()
const enhancers = [];
const middleware = [
  thunk,
  routerMiddleware(history),
  sagaMiddleware
];

const defaultState = {
  states: {
    loaded: false
  },
  ui : {
    theme: 'default'
  }
}

const devToolsExtension = window.devToolsExtension;
if (typeof devToolsExtension === 'function') {
  enhancers.push(devToolsExtension());
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
  RootReducer,
  defaultState,
  composedEnhancers
);

sagaMiddleware.run(RootSaga)

export default store;

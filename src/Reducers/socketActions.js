export const SOCKET_INIT = 'SOCKET/init'

export const socketInit = data => {
  return dispatch => {
    dispatch({
      type: SOCKET_INIT,
      data
    })
  }

}

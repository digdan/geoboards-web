export const APP_LOADED = 'APP/loaded'
export const APP_STATE = 'APP/state'
export const COMMAND_STATE_SHOW = 'COMMAND/state_show'

export const terminalDispatch = action => {
  return dispatch => {
    dispatch(action)
  }
}

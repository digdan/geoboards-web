import * as actions from './actions'
import _ from 'lodash'

export function StateReducer(state={}, action) {
  let newState = _.cloneDeep(state)
  switch (action.type) {
    case actions.APP_STATE : {
      newState.state = action.data
      return newState
    }
    case actions.APP_LOADED : {
      newState.loaded = action.data
      return newState
    }
    default : {
      return newState
    }
  }
}

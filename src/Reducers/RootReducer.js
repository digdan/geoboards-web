import { combineReducers } from 'redux'
import * as actions from './actions'
import { StateReducer } from './StateReducer'
import { UIReducer } from './UIReducer'
import _ from 'lodash'

const appReducer = combineReducers({
  states: StateReducer,
  ui: UIReducer
});

export const RootReducer = (state={}, action) => {
  return appReducer(state, action);
};

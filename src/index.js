import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {ThemeProvider, themes} from './Themes'
import {TranslateProvider, getLocale} from './Translate'
import store from './store'

ReactDOM.render((
  <Provider store={store}>
    <ThemeProvider theme={themes(store.getState().theme)}>
      <TranslateProvider locale={getLocale('en')}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </TranslateProvider>
    </ThemeProvider>    
  </Provider>
), document.getElementById('root'));
registerServiceWorker();

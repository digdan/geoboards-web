import React from 'react'
import PropTypes from 'prop-types'
import defaultTheme from './default'

const themes = theme => {
  switch (theme) {
    case 'default' : {
      return defaultTheme
    }
  }
  return defaultTheme
}

class ThemeProvider extends React.Component {
  static propTypes = {
    theme: PropTypes.object.isRequired
  }
  // you must specify what you’re adding to the context
  static childContextTypes = {
    theme: PropTypes.object.isRequired
  }
  getChildContext() {
    const { theme } = this.props
    return { theme }
  }
  render() {
    return React.Children.only(this.props.children)
  }
}

ThemeProvider.propTypes = {
  children: PropTypes.element
}

ThemeProvider.defaultProps = {
  children: []
}

const themed = ComponentToWrap => {
  return class ThemeComponent extends React.Component {
    static contextTypes = {
      theme: PropTypes.object.isRequired
    }
    render() {
      const { theme } = this.context
      return (
        <ComponentToWrap {...this.props} theme={theme} />
      )
    }
  }
}

export {
  ThemeProvider,
  themed,
  themes
}

import header from './header.module.styl'
import footer from './footer.module.styl'

const defaultTheme = Object.assign({},
  header,
  footer
)

export {
  defaultTheme as default
}
